package com.nurhadi.kitapsehat;

/**
 * Created by Kadek_P on 8/14/2017.
 */

import android.app.Activity;
import android.os.Bundle;

import com.nurhadi.kitapsehat.fragments.Camera2BasicFragment;

public class CameraActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        Bundle bundle = getIntent().getBundleExtra("bundle");
        String type = bundle.getString("type");
        if (null == savedInstanceState) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, Camera2BasicFragment.newInstance(type))
                    .commit();
        }
    }

}
