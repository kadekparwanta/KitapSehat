package com.nurhadi.kitapsehat.models

/**
 * Created by Kadek_P on 8/15/2017.
 */
enum class Organ {
    HEARTH,
    LIVER,
    LUNGS,
    KIDNEY,
    SPLEEN
}