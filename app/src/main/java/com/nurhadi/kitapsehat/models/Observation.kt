package com.nurhadi.kitapsehat.models

/**
 * Created by Kadek_P on 8/15/2017.
 */
class Observation(name: String? , condition: String?, indication: String?, suggestion: String?, affectedOrgan: Organ?) {
    var name = name
    var condition = condition
    var indication = indication
    var suggestion = suggestion
    var affectedOrgan = affectedOrgan
}