package com.nurhadi.kitapsehat.helpers

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.nurhadi.kitapsehat.R
import com.nurhadi.kitapsehat.models.Observation
import java.util.*
import android.util.DisplayMetrics




/**
 * Created by Kadek_P on 8/15/2017.
 */
class CustomSpinnerAdapter(context: Context, items: ArrayList<Observation>): ArrayAdapter<Observation>(context, R.layout.custom_spinner, items) {
    val mItems = items

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view =  super.getView(position, convertView, parent)
        val tv1 = view.findViewById(R.id.text1) as TextView
        tv1.setText(mItems.get(position).name)
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view =  super.getView(position, convertView, parent)
        val tv1 = view.findViewById(R.id.text1) as TextView
        tv1.setText(mItems.get(position).name)
        val metrics = parent!!.getResources().displayMetrics
        val dp = 40f
        val fpixels = metrics.density * dp
        val pixels = (fpixels + 0.5f).toInt()
        tv1.height = pixels
        return view
    }
}