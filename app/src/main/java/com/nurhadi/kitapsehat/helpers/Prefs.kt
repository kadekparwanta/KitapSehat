package com.nurhadi.kitapsehat.helpers

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by Kadek_P on 8/16/2017.
 */
class Prefs (context: Context) {
    val PREFS_FILENAME = "com.nurhadi.kitapsehat.prefs"
    val USER_EMAIL = "user_email"
    val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0);

    var userEmail: String
        get() = prefs.getString(USER_EMAIL, "")
        set(value) = prefs.edit().putString(USER_EMAIL, value).apply()
}