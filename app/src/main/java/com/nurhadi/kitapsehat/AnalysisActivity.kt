package com.nurhadi.kitapsehat

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.nurhadi.kitapsehat.fragments.analysis.EarFragment
import com.nurhadi.kitapsehat.fragments.analysis.EyeFragment
import com.nurhadi.kitapsehat.fragments.analysis.NoseFragment
import com.nurhadi.kitapsehat.fragments.analysis.TongueFragment

class AnalysisActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anaylisis)
        val bundle = intent.getBundleExtra("bundle")
        val type = bundle.get("type")
        val path = bundle.get("path") as String
        when(type) {
            "eye" -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.container, EyeFragment.newInstance(path))
                        .commit()
            }
            "nose" -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.container, NoseFragment.newInstance(path))
                        .commit()
            }
            "ear" -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.container, EarFragment.newInstance(path))
                        .commit()
            }
            "tongue" -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.container, TongueFragment.newInstance(path))
                        .commit()
            }
        }
    }
}
