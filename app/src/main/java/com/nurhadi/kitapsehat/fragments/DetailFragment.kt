package com.nurhadi.kitapsehat.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.nurhadi.kitapsehat.CameraActivity
import com.nurhadi.kitapsehat.R
import android.text.Spanned



/**
 * Created by Kadek_P on 8/14/2017.
 */
class DetailFragment: Fragment() {

    companion object {
        fun newInstance(type: String): DetailFragment {
            var args: Bundle = Bundle()
            args.putString("type", type)
            var homeFragment: DetailFragment = newInstance()
            homeFragment.arguments = args
            return homeFragment
        }

        fun newInstance(): DetailFragment {
            return DetailFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_item_detail, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val type = arguments.get("type")
        if (view != null) {
            val imageDetail: ImageView = view.findViewById(R.id.imageDetail) as ImageView
            val textDetail: TextView = view.findViewById(R.id.textDetail) as TextView
            val tvCondition: TextView = view.findViewById(R.id.tvCondition) as TextView
            when (type) {
                "hearth" -> {
                    activity.title = "Hearth Detail"
                    imageDetail.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.cardiology))
                    textDetail.setText("Fire is Yang in character. The organs associated with Fire element are The Heart (Yin) & the Small Intestine (Yang).")

                    val text = "Check your <font color=#FF0000>Heart</font> condition using the cart below !"
                    tvCondition.setText(fromHtml(text))
                }
                "kidney" -> {
                    activity.title = "Kidney Detail"
                    imageDetail.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.kidney))
                    textDetail.setText("Water is Yin in character. The organs associated with Water element are The Kidney (Yin) & Bladder (Yang).")
                    val text = "Check your <font color=#3498DB>Kidney</font> condition using the cart below !"
                    tvCondition.setText(fromHtml(text))
                }
                "liver" -> {
                    activity.title = "Liver Detail"
                    imageDetail.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.liver))
                    textDetail.setText("The element wood is masculine and considered less Yang than fire. The Liver (Yin) & gallbladder (Yang).")
                    val text = "Check your <font color=#E59866>Liver</font> condition using the cart below !"
                    tvCondition.setText(fromHtml(text))
                }
                "lungs" -> {
                    activity.title = "Lungs Detail"
                    imageDetail.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.lungs))
                    textDetail.setText("Fire is Yang in character. The organs associated with Fire element are The Heart (Yin) & the Small Intestine (Yang).")
                    val text = "Check your <font color=#7B7D7D>Lungs</font> condition using the cart below !"
                    tvCondition.setText(fromHtml(text))
                }
                "spleen" -> {
                    activity.title = "Spleen Detail"
                    imageDetail.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.spleen))
                    textDetail.setText("Fire is Yang in character. The organs associated with Fire element are The Heart (Yin) & the Small Intestine (Yang).")
                    val text = "Check your <font color=#44CC46>Spleen</font> condition using the cart below !"
                    tvCondition.setText(fromHtml(text))
                }
            }

            val card_eye : CardView = view.findViewById(R.id.card_eye) as CardView
            val card_nose : CardView = view.findViewById(R.id.card_nose) as CardView
            val card_ear : CardView = view.findViewById(R.id.card_ear) as CardView
            val card_tongue : CardView = view.findViewById(R.id.card_tongue) as CardView
            card_eye.setOnClickListener(mOnclickListener)
            card_nose.setOnClickListener(mOnclickListener)
            card_ear.setOnClickListener(mOnclickListener)
            card_tongue.setOnClickListener(mOnclickListener)
        }
    }

    val mOnclickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.card_eye -> {
                val intent = Intent(context,CameraActivity::class.java)
                val bundle = Bundle()
                bundle.putString("type","eye")
                intent.putExtra("bundle", bundle)
                activity.startActivity(intent)
            }
            R.id.card_nose -> {
                val intent = Intent(context,CameraActivity::class.java)
                val bundle = Bundle()
                bundle.putString("type","nose")
                intent.putExtra("bundle", bundle)
                activity.startActivity(intent)
            }
            R.id.card_ear -> {
                val intent = Intent(context,CameraActivity::class.java)
                val bundle = Bundle()
                bundle.putString("type","ear")
                intent.putExtra("bundle", bundle)
                activity.startActivity(intent)
            }
            R.id.card_tongue -> {
                val intent = Intent(context,CameraActivity::class.java)
                val bundle = Bundle()
                bundle.putString("type","tongue")
                intent.putExtra("bundle", bundle)
                activity.startActivity(intent)
            }
        }
    }

    fun fromHtml(html: String): Spanned {
        val result: Spanned
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            result = Html.fromHtml(html)
        }
        return result
    }
}