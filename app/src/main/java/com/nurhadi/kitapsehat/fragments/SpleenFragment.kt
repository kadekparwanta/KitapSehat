package com.nurhadi.kitapsehat.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.nurhadi.kitapsehat.R

/**
 * Created by Kadek_P on 8/14/2017.
 */
class SpleenFragment: Fragment() {
    companion object {
        fun newInstance() : SpleenFragment {
            return SpleenFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_spleen, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity.title = "Spleen | Earth"
        if (view != null) {
            val button : Button = view.findViewById(R.id.button) as Button
            button.setOnClickListener {
                val detailFragment = DetailFragment.newInstance("spleen")
                fragmentManager.beginTransaction()
                        .replace(R.id.container, detailFragment, detailFragment.javaClass.simpleName)
                        .addToBackStack(detailFragment.javaClass.simpleName).commit()
            }
        }
    }
}