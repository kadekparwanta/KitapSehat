package com.nurhadi.kitapsehat.fragments.analysis

import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.nurhadi.kitapsehat.R
import java.io.File

/**
 * Created by Kadek_P on 8/14/2017.
 */
class NoseFragment: Fragment() {
    companion object {
        fun newInstance(path: String): NoseFragment {
            var args: Bundle = Bundle()
            args.putString("path", path)
            var noseFragment: NoseFragment = newInstance()
            noseFragment.arguments = args
            return noseFragment
        }

        fun newInstance() : NoseFragment {
            return NoseFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_nose, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity.title = "Analysing Nose"
        if (view != null) {
            val foto = view.findViewById(R.id.foto) as ImageView
            val path = arguments.get("path") as String?
            var file = File(path)
            if (path  == null) {
                file = File(activity.getExternalFilesDir(null), "pic.jpg")
            }

            if (file.exists()) {
                foto.setImageURI(Uri.fromFile(file))
            }

            val seekbar: SeekBar = view.findViewById(R.id.seekBar) as SeekBar
            val scale = seekbar.progress / 10.0f + 1
            foto.scaleX = scale
            foto.scaleY = scale

            seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(p0: SeekBar?, progress: Int, b: Boolean) {
                    val scale = progress / 10.0f + 1
                    foto.scaleX = scale
                    foto.scaleY = scale
                }

                override fun onStartTrackingTouch(p0: SeekBar?) {

                }

                override fun onStopTrackingTouch(p0: SeekBar?) {

                }

            })

            Snackbar.make(activity.findViewById(R.id.myCoordinatorLayout), "Nose analysis is under construction, try to observe your eyes",
            Snackbar.LENGTH_LONG)
            .show();

            val diagnoseButton = view.findViewById(R.id.diagnose) as Button
            diagnoseButton.setOnClickListener {
                val diagnoseFragment = DiagnoseFragment.newInstance(path!!)
                fragmentManager.beginTransaction()
                        .replace(R.id.container, diagnoseFragment, diagnoseFragment.javaClass.simpleName)
                        .commit()
            }

            val spinner_eye: Spinner = view.findViewById(R.id.spinner_eye) as Spinner
            val adapter: ArrayAdapter<CharSequence> = ArrayAdapter.createFromResource(activity, R.array.eyes, android.R.layout.simple_spinner_item)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_eye.setAdapter(adapter);
        }
    }
}