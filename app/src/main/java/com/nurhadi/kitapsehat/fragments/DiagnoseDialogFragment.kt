package com.nurhadi.kitapsehat.fragments

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.content.DialogInterface
import android.R.string.cancel
import android.support.v7.app.AlertDialog
import com.nurhadi.kitapsehat.R


/**
 * Created by Kadek_P on 8/15/2017.
 */
class DiagnoseDialogFragment : DialogFragment() {
    var title: String? = null
    var message: String? = null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Use the Builder class for convenient dialog construction
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                    // FIRE ZE MISSILES!
                    this.dismiss()
                })
        // Create the AlertDialog object and return it
        return builder.create()
    }
}