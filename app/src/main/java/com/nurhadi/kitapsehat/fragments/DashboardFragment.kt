package com.nurhadi.kitapsehat.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.nurhadi.kitapsehat.R

/**
 * Created by Kadek_P on 8/14/2017.
 */
class DashboardFragment: Fragment() {
    companion object {
        fun newInstance() : DashboardFragment {
            return DashboardFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity.title = "Dashboard"
        if (view != null) {
            val btnHearth: Button = view.findViewById(R.id.btnHearth) as Button
            val btnSpleen: Button = view.findViewById(R.id.btnSpleen) as Button
            val btnLungs: Button = view.findViewById(R.id.btnLungs) as Button
            val btnKidney: Button = view.findViewById(R.id.btnKidney) as Button
            val btnLiver: Button = view.findViewById(R.id.btnLiver) as Button
            btnHearth.setOnClickListener(mOnclickListener)
            btnSpleen.setOnClickListener(mOnclickListener)
            btnLungs.setOnClickListener(mOnclickListener)
            btnKidney.setOnClickListener(mOnclickListener)
            btnLiver.setOnClickListener(mOnclickListener)

        }
    }

    val mOnclickListener: View.OnClickListener = View.OnClickListener { view ->
        val fragmentManager = activity.supportFragmentManager
        when (view.id) {
            R.id.btnHearth -> {
                val hearthFragment = HearthFragment.newInstance()
                fragmentManager.beginTransaction()
                        .replace(R.id.container, hearthFragment, hearthFragment.javaClass.simpleName)
                        .addToBackStack(hearthFragment.javaClass.simpleName).commit()
            }
            R.id.btnSpleen -> {
                val spleenFragment = SpleenFragment.newInstance()
                fragmentManager.beginTransaction()
                        .replace(R.id.container, spleenFragment, spleenFragment.javaClass.simpleName)
                        .addToBackStack(spleenFragment.javaClass.simpleName).commit()
            }
            R.id.btnLungs -> {
                val lungsFragment = LungsFragment.newInstance()
                fragmentManager.beginTransaction()
                        .replace(R.id.container, lungsFragment, lungsFragment.javaClass.simpleName)
                        .addToBackStack(lungsFragment.javaClass.simpleName).commit()
            }
            R.id.btnKidney -> {
                val kidneyFragment = KidneyFragment.newInstance()
                fragmentManager.beginTransaction()
                        .replace(R.id.container, kidneyFragment, kidneyFragment.javaClass.simpleName)
                        .addToBackStack(kidneyFragment.javaClass.simpleName).commit()
            }
            R.id.btnLiver -> {
                val liverFragment = LiverFragment.newInstance()
                fragmentManager.beginTransaction()
                        .replace(R.id.container, liverFragment, liverFragment.javaClass.simpleName)
                        .addToBackStack(liverFragment.javaClass.simpleName).commit()
            }
        }
    }
}