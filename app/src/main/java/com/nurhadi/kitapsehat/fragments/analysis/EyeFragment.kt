package com.nurhadi.kitapsehat.fragments.analysis

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.nurhadi.kitapsehat.R
import com.nurhadi.kitapsehat.fragments.DetailFragment
import com.nurhadi.kitapsehat.helpers.CustomSpinnerAdapter
import com.nurhadi.kitapsehat.models.Observation
import com.nurhadi.kitapsehat.models.Organ
import java.io.File
import java.util.HashSet

/**
 * Created by Kadek_P on 8/14/2017.
 */
class EyeFragment: Fragment() {

    companion object {
        fun newInstance(path: String): EyeFragment {
            var args: Bundle = Bundle()
            args.putString("path", path)
            var eyeFragment: EyeFragment = newInstance()
            eyeFragment.arguments = args
            return eyeFragment
        }

        fun newInstance() : EyeFragment {
            return EyeFragment()
        }
    }

    val eyeObservations = ArrayList<Observation>()
    var eyeDiagnose: Observation? = null
    var irisDiagnose: Observation? = null
    var scleraDiagnose: Observation? = null
    var pupilDiagnose: Observation? = null
    var eyelidDiagnose: Observation? = null

    val irisObservations = ArrayList<Observation>()
    var scleraObservations = ArrayList<Observation>()
    var pupilObservations = ArrayList<Observation>()
    val eyelidObservations = ArrayList<Observation>()

    var diagnosis = HashSet<Observation>()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_eye, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity.title = "Analysing Eyes"
        if (view != null) {
            val foto = view.findViewById(R.id.foto) as ImageView
            val path = arguments.get("path") as String?
            var file = File(path)
            if (path  == null) {
                file = File(activity.getExternalFilesDir(null), "pic.jpg")
            }

            if (file.exists()) {
                foto.setImageURI(Uri.fromFile(file))
            }

            val seekbar: SeekBar = view.findViewById(R.id.seekBar) as SeekBar
            val scale = seekbar.progress / 10.0f + 1
            foto.scaleX = scale
            foto.scaleY = scale

            seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(p0: SeekBar?, progress: Int, b: Boolean) {
                    val scale = progress / 10.0f + 1
                    foto.scaleX = scale
                    foto.scaleY = scale
                }

                override fun onStartTrackingTouch(p0: SeekBar?) {

                }

                override fun onStopTrackingTouch(p0: SeekBar?) {

                }

            })

            val diagnoseButton = view.findViewById(R.id.diagnose) as Button
            diagnoseButton.setOnClickListener {
                val diagnoseFragment = DiagnoseFragment.newInstance(path!!)
                diagnosis.add(eyeDiagnose!!)
                diagnosis.add(irisDiagnose!!)
                diagnosis.add(scleraDiagnose!!)
                diagnosis.add(pupilDiagnose!!)
                diagnosis.add(eyelidDiagnose!!)

                diagnoseFragment.diagnosis = diagnosis
                fragmentManager.beginTransaction()
                        .replace(R.id.container, diagnoseFragment, diagnoseFragment.javaClass.simpleName)
                        .commit()
            }

            val spinner_eye: Spinner = view.findViewById(R.id.spinner_eye) as Spinner
            eyeObservations.add(Observation("Pale","Lack of blood","Pale is an indication of the lack of blood in certain parts of the body, blood flow is caused by the flow of Qi. Qi weaken hearth organ so it is not enough to drain blood on the surface of the eyes census"," Bitter taste is a feeling that can strengthen the heart, fulfill the taste is expected to strengthen the heart Qi. Limit Kidney activity due to kidney representation Water and Heart Fire representation. Strengthens the heart because in Wu Xing theory Wood Liver live Heart Fire.",Organ.HEARTH))
            eyeObservations.add(Observation("Pink","Normal","Pink is a normal indication of Qi","",Organ.HEARTH))
            eyeObservations.add(Observation("Dark Red","Not Good","Rich Qi is characterized by the amount of blood in the eye cyst, the condition of Qi from the heart is enough or very strong.","The condition is too strong or excess Qi in the organ can be done by harmonizing by strengthening the organ elements that limit it. Like strengthening the element of renal water, or limiting the organ elements to reduce the activities that strengthen the heart",Organ.HEARTH))
            eyeDiagnose = eyeObservations[0]
            val eye_adapter = CustomSpinnerAdapter(activity, eyeObservations)
            spinner_eye.setAdapter(eye_adapter);
            spinner_eye.onItemSelectedListener = onItemSelectedListener

            val spinner_iris: Spinner = view.findViewById(R.id.spinner_iris) as Spinner
            irisObservations.add(Observation("Clear","Normal","The clear cornea is a representation of the condition of the liver in good condition","",Organ.LIVER))
            irisObservations.add(Observation("Dull","Liver is impaired","Eye view becomes blurred or dizzy due to the function of the cornea is reduced due to the relationship of the liver is impaired.","",Organ.LIVER))
            irisObservations.add(Observation("Whiten","Chronic disorders","Indications of corneal abnormality are due to chronic disorders of the function or damage to the liver organ","",Organ.LIVER))
            irisDiagnose = irisObservations[0]
            val iris_adapter = CustomSpinnerAdapter(activity, irisObservations)
            spinner_iris.setAdapter(iris_adapter);
            spinner_iris.onItemSelectedListener = onItemSelectedListener

            val spinner_sclera: Spinner = view.findViewById(R.id.spinner_sclera) as Spinner
            scleraObservations.add(Observation("White","Normal","Indicates the condition of the lungs in good condition","",Organ.LUNGS))
            scleraObservations.add(Observation("Stained","Poor","Indicates poor conditions of the lung, due to pathogens in the lungs, pathogens may be hot, cold or moist","",Organ.LUNGS))
            scleraObservations.add(Observation("Yellowish","Weak","Indicates a weak condition of the lung, where the yellow color is an overactive gastric representation that limits the lung. If the yellow color is not only on the eye sclera, but also appears on the whole yellowing body indicates a liver disorder","",Organ.LUNGS))
            scleraObservations.add(Observation("Blood vessel","Overactive","Indicates severe Qi lung due to overactive lung activity. Lung as a cardiac canopy, can also indicate the heart in the state of Re (Heat)","",Organ.LUNGS))
            scleraDiagnose = scleraObservations[0]
            val sclera_adapter = CustomSpinnerAdapter(activity, scleraObservations)
            spinner_sclera.setAdapter(sclera_adapter);
            spinner_sclera.onItemSelectedListener = onItemSelectedListener

            val spinner_pupil: Spinner = view.findViewById(R.id.spinner_pupil) as Spinner
            pupilObservations.add(Observation("Widened","Less Focus","Indications of Qi Kidney has weakened resulting in reduced pupil ability in limiting the rays into the eye so that the stares less focus. The effects of age also result in decreased Qi Kidney.","",Organ.KIDNEY))
            pupilObservations.add(Observation("Responsive to light","Normal","Indikasi Ginjal dalam keadaan baik karena Qi nya cukup untuk dialirkan pada susum tulang belakang pada saat pembentukan sel darah merah","",Organ.KIDNEY))
            pupilObservations.add(Observation("Unresponsive to light","Not Good","Indications Kidney in a state of no good, if in a state of illness that has been chronically unresponsive pupil reaction signifies a drastic decrease Qi in the body.","",Organ.KIDNEY))
            pupilDiagnose = pupilObservations[0]
            val pupil_adapter = CustomSpinnerAdapter(activity, pupilObservations)
            spinner_pupil.setAdapter(pupil_adapter);
            spinner_pupil.onItemSelectedListener = onItemSelectedListener

            val spinner_eyelid: Spinner = view.findViewById(R.id.spinner_eyelid) as Spinner
            eyelidObservations.add(Observation("Blackened","Normal","The function of the spleen weakens due to strong heart Qi, limiting the Heart Wood so that Qi spleen slightly.","",Organ.SPLEEN))
            eyelidObservations.add(Observation("Puffy eyes","Weak","The decrease in muscle strength due to the function of the spleen decreases, in this case the widening occurring in the eye bag is an indication of weak spleen condition","",Organ.SPLEEN))
            eyelidDiagnose = eyelidObservations[0]
            val eyelid_adapter = CustomSpinnerAdapter(activity, eyelidObservations)
            spinner_eyelid.setAdapter(eyelid_adapter);
            spinner_eyelid.onItemSelectedListener = onItemSelectedListener
        }
    }

    val onItemSelectedListener =  object: AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(p0: AdapterView<*>?) {

        }

        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            when (p0!!.id) {
                R.id.spinner_eye -> {
                    eyeDiagnose = eyeObservations.get(p2)
                }

                R.id.spinner_iris -> {
                    irisDiagnose = irisObservations.get(p2)
                }

                R.id.spinner_sclera -> {
                    scleraDiagnose = scleraObservations.get(p2)
                }

                R.id.spinner_pupil -> {
                    pupilDiagnose = pupilObservations.get(p2)
                }

                R.id.spinner_eyelid -> {
                    eyelidDiagnose = eyelidObservations.get(p2)
                }
            }
        }

    }
}