package com.nurhadi.kitapsehat.fragments.analysis

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Html
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.nurhadi.kitapsehat.R
import com.nurhadi.kitapsehat.fragments.DiagnoseDialogFragment
import com.nurhadi.kitapsehat.models.Observation
import com.nurhadi.kitapsehat.models.Organ
import java.io.File

/**
 * Created by Kadek_P on 8/14/2017.
 */
class DiagnoseFragment: Fragment() {
    companion object {
        fun newInstance(path: String): DiagnoseFragment {
            var args: Bundle = Bundle()
            args.putString("path", path)
            var diagnoseFragment: DiagnoseFragment = newInstance()
            diagnoseFragment.arguments = args
            return diagnoseFragment
        }

        fun newInstance() : DiagnoseFragment {
            return DiagnoseFragment()
        }
    }

    var diagnosis = HashSet<Observation>()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_diagnose, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity.title = "Diagnose"
        if (view != null) {
            val foto = view.findViewById(R.id.foto) as ImageView
            val path = arguments.get("path") as String?
            var file = File(path)
            if (path  == null) {
                file = File(activity.getExternalFilesDir(null), "pic.jpg")
            }

            if (file.exists()) {
                foto.setImageURI(Uri.fromFile(file))
            }

            val text = "<font color=#33CCFF>Details</font>"

            for (item:Observation in diagnosis) {
                when (item.affectedOrgan) {
                    Organ.HEARTH -> {
                        val hearth_condition = view.findViewById(R.id.hearth_condition) as TextView
                        val hearth_suggestion = view.findViewById(R.id.hearth_suggestion) as TextView
                        hearth_condition.setText(item.condition)
                        hearth_suggestion.setText(fromHtml(text))
                        hearth_suggestion.setOnClickListener {
                            var dialog = DiagnoseDialogFragment()
                            dialog.title = "Hearth" + " : " + item.condition
                            dialog.message = item.indication
                            dialog.show(fragmentManager,"Hearth Condition")
                        }
                    }

                    Organ.LIVER -> {
                        val liver_condition = view.findViewById(R.id.liver_condition) as TextView
                        val liver_suggestion = view.findViewById(R.id.liver_suggestion) as TextView
                        liver_condition.setText(item.condition)
                        liver_suggestion.setText(fromHtml(text))
                        liver_suggestion.setOnClickListener {
                            var dialog = DiagnoseDialogFragment()
                            dialog.title = "Liver" + " : " + item.condition
                            dialog.message = item.indication
                            dialog.show(fragmentManager,"Liver Condition")
                        }
                    }

                    Organ.LUNGS -> {
                        val lungs_condition = view.findViewById(R.id.lungs_condition) as TextView
                        val lungs_suggestion = view.findViewById(R.id.lungs_suggestion) as TextView
                        lungs_condition.setText(item.condition)
                        lungs_suggestion.setText(fromHtml(text))
                        lungs_suggestion.setOnClickListener {
                            var dialog = DiagnoseDialogFragment()
                            dialog.title = "Lungs" + " : " + item.condition
                            dialog.message = item.indication
                            dialog.show(fragmentManager,"Lungs Condition")
                        }
                    }

                    Organ.KIDNEY -> {
                        val kidney_condition = view.findViewById(R.id.kidney_condition) as TextView
                        val kidney_suggestion = view.findViewById(R.id.kidney_suggestion) as TextView
                        kidney_condition.setText(item.condition)
                        kidney_suggestion.setText(fromHtml(text))
                        kidney_suggestion.setOnClickListener {
                            var dialog = DiagnoseDialogFragment()
                            dialog.title = "Kidney" + " : " + item.condition
                            dialog.message = item.indication
                            dialog.show(fragmentManager,"Kidney Condition")
                        }
                    }

                    Organ.SPLEEN -> {
                        val spleen_condition = view.findViewById(R.id.spleen_condition) as TextView
                        val spleen_suggestion = view.findViewById(R.id.spleen_suggestion) as TextView
                        spleen_condition.setText(item.condition)
                        spleen_suggestion.setText(fromHtml(text))
                        spleen_suggestion.setOnClickListener {
                            var dialog = DiagnoseDialogFragment()
                            dialog.title = "Spleen" + " : " + item.condition
                            dialog.message = item.indication
                            dialog.show(fragmentManager,"Spleen Condition")
                        }
                    }

                }
            }
        }
    }

    fun fromHtml(html: String): Spanned {
        val result: Spanned
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            result = Html.fromHtml(html)
        }
        return result
    }
}